package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {
        Connection connection = null;

        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String startDate, String finishDate) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;

        try {
            connection = getConnection();

            Integer id = null;

            if(!StringUtils.isBlank(userId) && (userId.matches("^[0-9]+$"))) {
              id = Integer.parseInt(userId);
            }

            if(!StringUtils.isEmpty(startDate)) {
            	startDate = startDate + " 00:00:00";
            } else {
            	startDate = "2022-04-01 08:00:00";
            }

            if(!StringUtils.isEmpty(finishDate)) {
            	finishDate = finishDate + " 23:59:59";
            } else {
            	Date dateObj = new Date();
            	finishDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateObj);
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, startDate, finishDate, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //つぶやきを編集する仕様（取得）
    public Message select(int messageId) {
        Connection connection = null;

        try {
            connection = getConnection();

            Message editMessage = new MessageDao().select(connection, messageId);
            commit(connection);

            return editMessage;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //つぶやきを編集する仕様（登録）
    public void update(Message newMessage) {
        Connection connection = null;

        try {
            connection = getConnection();

            new MessageDao().update(connection, newMessage);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //つぶやきを削除する仕様
    public void delete(int id) {
        Connection connection = null;

        try {
            connection = getConnection();
            new MessageDao().delete(connection, id);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}