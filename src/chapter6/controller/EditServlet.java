package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		String beforeMessageId = request.getParameter("message_id");

		List<String> errorMessages = new ArrayList<String>();

		Message	editMessage = null;

		if(!(StringUtils.isBlank(beforeMessageId)) && (beforeMessageId.matches("^[0-9]+$"))) {
			int afterMessageId = Integer.parseInt(beforeMessageId);
			editMessage = new MessageService().select(afterMessageId);
		}

		if(editMessage == null){
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", editMessage);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();

		int messageId = Integer.parseInt(request.getParameter("message_id"));
		String text = request.getParameter("text");

        if (!isValid(text, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("text", text);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }

        Message newMessage = new Message();
        newMessage.setId(messageId);
        newMessage.setText(text);

        new MessageService().update(newMessage);

        response.sendRedirect("./");
    }

	private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
